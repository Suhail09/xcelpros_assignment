import 'package:assignment/src/helpers/utility.dart';
import 'package:flutter/material.dart';

class AppTextWidget extends StatelessWidget {
  const AppTextWidget({
    required  this.title,
     this.textColor = Colors.black,
    this.fontWeight = FontWeight.w400,
    this.fontSize = 14.0,
    this.maxLines = 10
});

  final String title;
  final Color textColor;
  final FontWeight fontWeight;
  final double fontSize;
  final int maxLines;
  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      maxLines: maxLines,
      textScaleFactor: Utility.textScaleFactor,
      style: Theme.of(context).textTheme.headline6?.copyWith(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: textColor),
    );
  }
}
