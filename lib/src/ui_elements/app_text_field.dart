
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:flutter/material.dart';

typedef void OnTextChange(String value);

class AppTextField extends StatefulWidget {

  AppTextField({
    this.controller,
    this.onTextChange,
    this.hintText = '',
    this.isSecureText = false,
    this.secureTextChar = '*',
    this.textStyle = const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
    this.errorText,
    this.noErrorLine = 1,
    this.prefixWidget,
  });

  @override
  _AppTextFieldState createState() => _AppTextFieldState();

  TextEditingController? controller;
  TextStyle textStyle;
  OnTextChange? onTextChange;
  String hintText;
  String? errorText;
  bool isSecureText;
  String secureTextChar;
  int noErrorLine;
  Widget? prefixWidget;
}

class _AppTextFieldState extends State<AppTextField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      child: Row(
        children: [
          if(widget.prefixWidget != null)
            Expanded(flex: 2, child: widget.prefixWidget ?? Container()),
          Expanded(
            flex: 9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: TextFormField(
                    controller: widget.controller,
                    style: widget.textStyle,
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black26, width: 2),
                      ),
                      border: UnderlineInputBorder(
                      ),
                      hintText: widget.hintText,
                      contentPadding: EdgeInsets.only(left: 10,right: 10,top: 10),
                      errorMaxLines: widget.noErrorLine,
                    ),
                    obscureText: widget.isSecureText,
                    obscuringCharacter: widget.secureTextChar,
                    onChanged: widget.onTextChange  ?? null,
                  ),
                ),
                _errorMessage(widget.errorText ?? '')
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _errorMessage(String message) {
    return Container(
      margin: EdgeInsets.all(message.isEmpty ? 0 : 5),
      child: AppTextWidget(
        title: message,
        textColor: Colors.red,
      ),
    );
  }
}
