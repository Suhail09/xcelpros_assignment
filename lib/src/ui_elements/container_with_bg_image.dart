
import 'package:assignment/src/constants/app_colors.dart';
import 'package:flutter/material.dart';

class ContainerWithBackgroundImage extends StatelessWidget {

  const ContainerWithBackgroundImage({required this.child, this.imagePath = ''});
  final  Widget child;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              AppColor.appBGColorPrimary,
              AppColor.appBGColorSecondary,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp
        ),
        image:  imagePath.isNotEmpty ? DecorationImage(
          image: AssetImage(imagePath),
          fit: BoxFit.contain,
          alignment: Alignment.topRight
        ) : null,
      ),
      child: child,
    );
  }
}
