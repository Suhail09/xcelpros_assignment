import 'package:assignment/src/app_flow/auth_flow/login/login_bloc.dart';
import 'package:assignment/src/app_flow/auth_flow/sign_up/sign_up_bloc.dart';
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/helpers/utility.dart';
import 'package:assignment/src/my_app.dart';
import 'package:assignment/src/ui_elements/app_text_field.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:assignment/src/ui_elements/container_with_bg_image.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();

  final SignupBloc _bloc = SignupBloc();
}

class _SignUpScreenState extends State<SignUpScreen>  with SingleTickerProviderStateMixin, RouteAware{

  TextEditingController _userNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  late AnimationController controller;
  late Animation aspectRatioAnimation;

  @override
  void initState() {
    controller =  AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    aspectRatioAnimation = Tween<double>(begin: 1.0, end: 2.0).animate(controller);

    controller.addListener(() {
      widget._bloc.animationObserver.sink.add(true);
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
  }

  @override
  void didPush() {
    controller.forward();
  }

  @override
  void didPopNext() {
    controller.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ContainerWithBackgroundImage(
        imagePath: AppImagesKeys.backgroundImage.value,
        child: Container(
          child: Column(
            children: [
              topSection(),
              signUpFormWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget topSection() {
    return StreamBuilder<bool>(
        stream: widget._bloc.animationObserver.stream,
        initialData: true,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          return AspectRatio(
            aspectRatio: aspectRatioAnimation.value,
            child: SafeArea(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Row(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: (){
                                Navigator.pop(context);
                              },
                                child: Icon(Icons.arrow_back_ios, color: Colors.white,)),
                            InkWell(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: _textWidget(title:LanguageHandler().localize(AppStaticTextKeys.backText.value,),
                                  textColor: Colors.white, fontSize: 20,fontWeight: FontWeight.w700 ),
                            ),
                          ],
                        ),

                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        padding: EdgeInsets.all(20),
                        child: _textWidget(title:LanguageHandler().localize(AppStaticTextKeys.createNewAccountText.value,),
                            textColor: Colors.white, fontSize: 20,fontWeight: FontWeight.w700 ),
                      ),
                    ),
                  ],

                ),
              ),
            ),
          );
        }
    );
  }

  Widget signUpFormWidget() {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40)
            )
        ),
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              userNameTextField(),
              passwordTextField(),
              emailTextField(),
              termsAndConditionsWidget(),
              submitButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget userNameTextField() {
    return Container(
      margin: EdgeInsets.only(top: 50, bottom: 5),
      child: StreamBuilder<String>(
          stream: widget._bloc.userNameStream,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            String errorMessage = snapshot.data ?? '';
            return AppTextField(
              controller: _userNameController,
              hintText: LanguageHandler().localize(AppStaticTextKeys.userNameText.value),
              prefixWidget: Icon(Icons.person_outline),
              onTextChange: widget._bloc.onUserNameChange,
              errorText: errorMessage,
            );
          }),
    );
  }

  Widget passwordTextField() {
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 10),
      child: StreamBuilder<String>(
          stream: widget._bloc.passwordStream,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            String errorMessage = snapshot.data ?? '';
            return AppTextField(
              controller: _passwordController,
              hintText: LanguageHandler().localize(AppStaticTextKeys.passwordText.value),
              prefixWidget: Icon(Icons.lock_outline_sharp),
              isSecureText: true,
              onTextChange: widget._bloc.onPasswordChange,
              errorText: errorMessage,
              noErrorLine: 2,
            );
          }),
    );
  }

  Widget emailTextField() {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 5),
      child: StreamBuilder<String>(
          stream: widget._bloc.emailStream,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            String errorMessage = snapshot.data ?? '';
            return AppTextField(
              controller: _emailController,
              hintText: LanguageHandler().localize(AppStaticTextKeys.emailText.value),
              prefixWidget: Icon(Icons.email_outlined),
              onTextChange: widget._bloc.onEmailChange,
              errorText: errorMessage,
            );
          }),
    );
  }

  Widget termsAndConditionsWidget() {
    return Container(
      margin: EdgeInsets.only( top: 50, left: 10),
      alignment: Alignment.center,
      child: Row(
        children: [
          StreamBuilder<bool>(
            stream: widget._bloc.termsConditionAcceptedStream,
            initialData: false,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              bool hasAccepted = snapshot.data ?? false;
              return IconButton(onPressed: widget._bloc.onTermsAcceptanceChange,
                  icon:Icon(hasAccepted ? Icons.check_box : Icons.check_box_outline_blank,
                  color: hasAccepted ? Theme.of(context).primaryColor : Colors.grey ,
                  ));
            }
          ),
          Expanded(
            child: Wrap(
              alignment: WrapAlignment.start,
              children: [
                _textWidget(title:LanguageHandler().localize(AppStaticTextKeys.iHaveAcceptedTheText.value,),
                    textColor: Colors.black54, fontSize: 15),
                InkWell(
                    onTap: () {
                      ///TODO: Handle Terms & Conditions action
                    },
                    child: _textWidget(title: LanguageHandler().localize(AppStaticTextKeys.termsAndConditionText.value),
                      fontSize: 15,fontWeight: FontWeight.w700, textColor: Theme.of(context).primaryColor,
                      decoration: TextDecoration.underline
                    )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }



  Widget submitButton() {
    return StreamBuilder<bool>(
        stream: widget._bloc.submitButtonStream,
        initialData: false,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          bool isValid = snapshot.data ?? false;
          return Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.width * 0.15,
            margin: EdgeInsets.only(top: 40),
            child: ElevatedButton(
              onPressed: isValid
                  ? signUpAction
                  : null,
              child: AppTextWidget(
                title: LanguageHandler().localize(AppStaticTextKeys.signUpText.value),
              ),
            ),
          );
        });
  }

  Widget _textWidget({required String title,
    Color textColor = Colors.black,
    FontWeight fontWeight = FontWeight.w400,
    double fontSize = 14.0, TextDecoration decoration = TextDecoration.none
  }) {

    return Text(title,
      textScaleFactor: Utility.textScaleFactor,
      style: Theme.of(context).textTheme.headline6?.copyWith(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: textColor,
        decoration: decoration,
      ),
    );
  }

  void signUpAction(){
    print('signin action');
  }


  @override
  void dispose() {
    widget._bloc.dispose();
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
