
import 'package:assignment/src/app_flow/base_bloc.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:rxdart/rxdart.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/helpers/validator.dart';

class SignupBloc extends BaseBloc {

  SignupBloc();


  final BehaviorSubject<String> _userName = BehaviorSubject<String>();
  final BehaviorSubject<String> _password = BehaviorSubject<String>();
  final BehaviorSubject<String> _email = BehaviorSubject<String>();

  Stream<String> get userNameStream => _userName.stream;
  Stream<String> get passwordStream => _password.stream;
  Stream<String> get emailStream => _email.stream;

  final PublishSubject<bool> _submitButtonSubject = PublishSubject<bool>();
  Stream<bool> get submitButtonStream => _submitButtonSubject.stream;

  final PublishSubject<bool> _termsConditionAccepted = PublishSubject<bool>();
  Stream<bool> get termsConditionAcceptedStream => _termsConditionAccepted.stream;

  bool _isUserNameValid = false;
  bool _isPasswordValid = false;
  bool _isEmailValid = false;
  bool _hasAcceptedTerms = false;

  bool get isUserInputValid => _isUserNameValid && _isPasswordValid && _isEmailValid && _hasAcceptedTerms;

  final PublishSubject<bool> animationObserver = PublishSubject<bool>();

  void onUserNameChange(String value) {
    _isUserNameValid = false;
    if(value.trim().isEmpty)
      _userName.sink.add(LanguageHandler().localize(AppStaticTextKeys.userNameRequiredMessage.value));
    else if(value.length < 5)
      _userName.sink.add(LanguageHandler().localize(AppStaticTextKeys.userNameInvalidMessage.value));
    else{
      _isUserNameValid = true;
      _userName.sink.add('');
    }
    _submitButtonSubject.sink.add(isUserInputValid);
  }

  void onPasswordChange(String value) {
    print(value);
    _isPasswordValid = false;
    if(value.trim().isEmpty)
      _password.sink.add(LanguageHandler().localize(AppStaticTextKeys.passwordRequiredMessage.value));

    else if(!value.isValidPassword())
      _password.sink.add(LanguageHandler().localize(AppStaticTextKeys.passwordStrengthErrorMessage.value));
    else{
      _isPasswordValid = true;
      _password.sink.add('');
    }
    _submitButtonSubject.sink.add(isUserInputValid);
  }

  void onEmailChange(String value) {
    _isEmailValid = false;
    if(value.trim().isEmpty)
      _email.sink.add(LanguageHandler().localize(AppStaticTextKeys.emailRequired.value));
    else if(!value.isValidEmail())
      _email.sink.add(LanguageHandler().localize(AppStaticTextKeys.emailInvalid.value));
    else{
      _isEmailValid = true;
      _email.sink.add('');
    }
    _submitButtonSubject.sink.add(isUserInputValid);
  }

  void onTermsAcceptanceChange(){
    _hasAcceptedTerms = !_hasAcceptedTerms;
    _termsConditionAccepted.sink.add(_hasAcceptedTerms);
    _submitButtonSubject.sink.add(isUserInputValid);
  }

  @override
  void dispose() {
    _submitButtonSubject.close();
    _userName.close();
    _password.close();
    animationObserver.close();
    _termsConditionAccepted.close();
    _email.close();
  }
}