import 'package:assignment/src/app_flow/auth_flow/login/login_bloc.dart';
import 'package:assignment/src/app_flow/auth_flow/sign_up/sign_up_screen.dart';
import 'package:assignment/src/app_flow/loader_widget.dart';
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/helpers/navigation_transition.dart';
import 'package:assignment/src/helpers/utility.dart';
import 'package:assignment/src/my_app.dart';
import 'package:assignment/src/ui_elements/app_text_field.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:assignment/src/ui_elements/container_with_bg_image.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget  {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();

  final LoginBloc _bloc = LoginBloc();
}

class _LoginScreenState extends State<LoginScreen> with SingleTickerProviderStateMixin, RouteAware{

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  late AnimationController controller;
  late Animation aspectRatioAnimation;

  @override
  void initState() {
    controller =  AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    aspectRatioAnimation = Tween<double>(begin: 1.0, end: 2.0).animate(controller);

    controller.addListener(() {
      widget._bloc.animationObserver.sink.add(true);
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
  }

  @override
  void didPopNext() {
    Future.delayed(Duration(milliseconds: 100),(){
      controller.reverse();
    });
  }

  @override
  Widget build(BuildContext context) {
    Utility.fontScaleFactor(context: context);
    return Scaffold(
      body: ContainerWithBackgroundImage(
        imagePath: AppImagesKeys.backgroundImage.value,
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              topSection(),
              loginFormWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget topSection() {
    return StreamBuilder<bool>(
        stream: widget._bloc.animationObserver.stream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          print(snapshot.data);
          return AspectRatio(
            aspectRatio: aspectRatioAnimation.value,
            child: Container(
              padding: EdgeInsets.all(20),
              //color: Colors.blue,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10, bottom: 15),
                    height: 4, width: MediaQuery.of(context).size.width*0.13,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(2)
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: _textWidget(title:LanguageHandler().localize(AppStaticTextKeys.welcomeText.value,),
                        textColor: Colors.white, fontSize: 20,fontWeight: FontWeight.w700 ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: _textWidget(title:LanguageHandler().localize(AppStaticTextKeys.toRoomControlText.value,),
                        textColor: Colors.white, fontSize: 20, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  Widget loginFormWidget() {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40)
            )
        ),
        padding: EdgeInsets.only(left: 20, right: 20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              userNameTextField(),
              passwordTextField(),
              submitButton(),
              signUpSection(),
            ],
          ),
        ),
      ),
    );
  }

  Widget userNameTextField() {
    return Container(
      margin: EdgeInsets.only(bottom: 5, top: 40),
      child: StreamBuilder<String>(
          stream: widget._bloc.userNameStream,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            String errorMessage = snapshot.data ?? '';
            return AppTextField(
              controller: _emailController,
              hintText: LanguageHandler().localize(AppStaticTextKeys.userNameText.value),
              prefixWidget: Icon(Icons.person_outline),
              onTextChange: widget._bloc.onUserNameChange,
              errorText: errorMessage,
            );
          }),
    );
  }

  Widget passwordTextField() {
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 10),
      child: StreamBuilder<String>(
          stream: widget._bloc.passwordStream,
          builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
            String errorMessage = snapshot.data ?? '';
            return AppTextField(
              controller: _passwordController,
              hintText: LanguageHandler().localize(AppStaticTextKeys.passwordText.value),
              prefixWidget: Icon(Icons.lock_outline_sharp),
              isSecureText: true,
              onTextChange: widget._bloc.onPasswordChange,
              errorText: errorMessage,
              noErrorLine: 2,
            );
          }),
    );
  }

  Widget submitButton() {
    return StreamBuilder<bool>(
        stream: widget._bloc.submitButtonStream,
        initialData: false,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          bool isValid = snapshot.data ?? false;
          return Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.width * 0.15,
            margin: EdgeInsets.only(top: 40),
            child: ElevatedButton(
              onPressed: isValid
                  ? signInAction
                  : null,
              child: AppTextWidget(
                title: LanguageHandler().localize(AppStaticTextKeys.signInText.value),
              ),
            ),
          );
        });
  }

  Widget signUpSection() {
    return Container(
      margin: EdgeInsets.only( top: 50,bottom: 20),
      alignment: Alignment.center,
      child: Wrap(
        alignment: WrapAlignment.center,
        children: [
          _textWidget(title:LanguageHandler().localize(AppStaticTextKeys.dontHaveAccountMessage.value,),
              textColor: Colors.black54, fontSize: 13),
          InkWell(
            onTap: () {
              controller.forward();
              Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (_, __, ___) => SignUpScreen(),
                  transitionDuration: Duration(seconds: 0),
                ),
              );
            },
            child: AppTextWidget(
              title: ' ' + LanguageHandler().localize(AppStaticTextKeys.signUpText.value),
              textColor: Theme.of(context).primaryColor,
              fontSize: 13,
              fontWeight: FontWeight.w600,
            ),
          )
        ],
      ),
    );
  }

  Widget _textWidget({required String title,
    Color textColor = Colors.black,
    FontWeight fontWeight = FontWeight.w400,
    double fontSize = 14.0
  }) {

    return AppTextWidget(
      title: title,
      textColor: textColor,
      fontSize: fontSize,
      fontWeight: fontWeight,
    );
  }

  void signInAction(){
    Navigator.of(context).push<dynamic>(
        CustomNavigationTransition(child: LoaderWidget(), type: NavigationTransitionType.upToDown,
            duration: const Duration(milliseconds: 300)
        )
    );
  }

  @override
  void dispose() {
    widget._bloc.dispose();
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
