
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:rxdart/rxdart.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/helpers/validator.dart';

import '../../base_bloc.dart';



class LoginBloc extends BaseBloc{

  final BehaviorSubject<String> _userName = BehaviorSubject<String>();
  final BehaviorSubject<String> _password = BehaviorSubject<String>();

  Stream<String> get userNameStream => _userName.stream;
  Stream<String> get passwordStream => _password.stream;

  final PublishSubject<bool> _submitButtonSubject = PublishSubject<bool>();
  Stream<bool> get submitButtonStream => _submitButtonSubject.stream;

  final PublishSubject<bool> animationObserver = PublishSubject<bool>();

  bool _isUserNameValid = false;
  bool _isPasswordValid = false;
  bool get isUserInputValid => _isUserNameValid && _isPasswordValid;


  void onUserNameChange(String value) {
    _isUserNameValid = false;
    if(value.trim().isEmpty)
      _userName.sink.add(LanguageHandler().localize(AppStaticTextKeys.userNameRequiredMessage.value));
    else if(value.length < 5)
      _userName.sink.add(LanguageHandler().localize(AppStaticTextKeys.userNameInvalidMessage.value));
    else{
      _isUserNameValid = true;
      _userName.sink.add('');
    }
    _submitButtonSubject.sink.add(isUserInputValid);
  }

  void onPasswordChange(String value) {
    print(value);
    _isPasswordValid = false;
    if(value.trim().isEmpty)
      _password.sink.add(LanguageHandler().localize(AppStaticTextKeys.passwordRequiredMessage.value));

    else if(!value.isValidPassword())
      _password.sink.add(LanguageHandler().localize(AppStaticTextKeys.passwordStrengthErrorMessage.value));
    else{
      _isPasswordValid = true;
      _password.sink.add('');
    }
    _submitButtonSubject.sink.add(isUserInputValid);
  }

  @override
  void dispose() {
    _submitButtonSubject.close();
    _userName.close();
    _password.close();
    animationObserver.close();
  }
}