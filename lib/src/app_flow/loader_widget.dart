import 'package:assignment/src/app_flow/main_flow/control_panel/control_panel_screen.dart';
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:assignment/src/ui_elements/container_with_bg_image.dart';
import 'package:flutter/material.dart';

class LoaderWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 2),() {
      openMainScreen(context);
    });
    return Scaffold(
      body: ContainerWithBackgroundImage(
        imagePath: AppImagesKeys.backgroundImage2.value,
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),

              AspectRatio(
                aspectRatio: 1.6,
                child: Container(
                  padding: EdgeInsets.only(top: 40),
                  alignment: Alignment.topCenter,

                  child: AppTextWidget(
                    title: LanguageHandler().localize(AppStaticTextKeys.letsGetYouStarted.value,),
                    textColor: Colors.white,
                    fontSize: 20,
                  ),

                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void openMainScreen(BuildContext context) {
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (_) => ControlPanelScreen()), (route) => false);
  }
}
