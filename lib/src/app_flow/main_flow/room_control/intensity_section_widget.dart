
import 'package:assignment/src/constants/app_colors.dart';
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:flutter/material.dart';

class IntensitySectionWidget extends StatelessWidget {
  const IntensitySectionWidget({Key? key,
    required this.onSliderValueChange,
    this.sliderValue = 0.0,
    this.maxSliderValue = 1.0,
    this.minSliderValue= 0.0,
    this.totalDivisions

  }) : super(key: key);

  final Function(double) onSliderValueChange;
  final double sliderValue;
  final double minSliderValue;
  final double maxSliderValue;
  final int? totalDivisions;


  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTextWidget(
              title: LanguageHandler().localize(AppStaticTextKeys.intensityText.value),
              textColor: AppColor.appBGColorSecondary,
              fontSize: 20,
              fontWeight:  FontWeight.w600,
            ),
            Row(
              children: [
                Image.asset(AppImagesKeys.bulbWhite.value),
                Expanded(
                  child: Container(
                    child: Slider(
                      value: sliderValue,
                      onChanged: onSliderValueChange,
                      min: minSliderValue,
                      max: maxSliderValue,
                      activeColor: Colors.orangeAccent,
                      divisions: totalDivisions,
                    ),
                  ),
                ),
                Image.asset(AppImagesKeys.bulbGlowed.value),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
