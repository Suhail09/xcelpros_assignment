
import 'package:assignment/src/constants/app_colors.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/network/models/scenes_model.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:flutter/material.dart';

class ScenesSectionWidget extends StatelessWidget {

  ScenesSectionWidget({Key? key,}) : super(key: key) {
    scenesModel = ScenesModel();
  }

  late ScenesModel scenesModel;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Container(
        width: double.maxFinite,
        constraints: BoxConstraints(
          minHeight: 200,
          maxHeight: 300
        ),
        margin: EdgeInsets.only(top: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTextWidget(
              title: LanguageHandler().localize(AppStaticTextKeys.scenesText.value),
              textColor: AppColor.appBGColorSecondary,
              fontSize: 20,
              fontWeight:  FontWeight.w600,
            ),

            Expanded(
              child: Container(
                child: Wrap(
                  children: _sceneWidgetList(context),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  List<Widget> _sceneWidgetList(BuildContext context ) {
    List<Widget> widgets = [];
    for(AScene  aScene in scenesModel.scenesList) {
      widgets.add(aSceneWidget(aScene: aScene,context: context));
    }
    return widgets;
  }


  Widget aSceneWidget({required AScene aScene, required BuildContext context}) {
    return Container(
      margin: EdgeInsets.all( 10),
      width: MediaQuery.of(context).size.width * 0.37,
      //height: 80,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: aScene.sceneGradientColors.first,
          borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
            colors: aScene.sceneGradientColors,
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            //stops: [0.0, 1.0],
            tileMode: TileMode.clamp
        ),
      ),
      child: AspectRatio(
        aspectRatio: 2,
        child: Center(
          child: Text(aScene.title,
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16),
          ),
        ),
      ),
    );
  }
}
