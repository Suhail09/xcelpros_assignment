
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import '../../base_bloc.dart';


class RoomControlBloc extends BaseBloc{

  RoomControlBloc() {
    colorsList = [];
    for(Color color in Colors.accents){
      colorsList.add(color);
    }
    lightColorSelected = colorsList.last;
  }

  final PublishSubject<bool> bulbArmAnimation = PublishSubject<bool>();
  final PublishSubject<bool> lightTypeUpdatePositionObserver = PublishSubject<bool>();
  final PublishSubject<bool> colorListAnimatorObserver = PublishSubject<bool>();

  final PublishSubject<double> _sliderValueChangeObserver = PublishSubject<double>();
  Stream<double> get sliderValueChangeStream => _sliderValueChangeObserver.stream;

  final PublishSubject<Color> _lightColorSelectionObserver = PublishSubject<Color>();
  Stream<Color> get lightColorSelectionStream => _lightColorSelectionObserver.stream;

  late List<Color> colorsList;
  double lightIntensity = 700.0;
  //late Color lightColorSelected;
  Color lightColorSelected = Colors.accents.first;

  Future<void> fetchData() {
    return Future.delayed(Duration(seconds: 1),() {
      return;
    });
  }

  void sliderValueDidChange(double value) {
    lightIntensity = value;
    _sliderValueChangeObserver.sink.add(value);
    _lightColorSelectionObserver.sink.add(lightColorSelected);
  }

  void didChangeColor(Color color){
    lightColorSelected = color;
    _lightColorSelectionObserver.sink.add(color);
  }

  @override
  void dispose() {
    bulbArmAnimation.close();
    lightTypeUpdatePositionObserver.close();
    _sliderValueChangeObserver.close();
    _lightColorSelectionObserver.close();
    colorListAnimatorObserver.close();
  }
}