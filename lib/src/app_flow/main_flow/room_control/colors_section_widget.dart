
import 'package:assignment/src/constants/app_colors.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:flutter/material.dart';

class ColorSectionWidget extends StatelessWidget {

  const ColorSectionWidget({Key? key,
    required this.colorsList,
    required this.onColorSelection,
    this.contentPadding = 0
  }) : super(key: key);

  final List<Color> colorsList;
  final Function(Color) onColorSelection;
  final double contentPadding;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(top: 30),
        height: 90,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppTextWidget(
              title: LanguageHandler().localize(AppStaticTextKeys.colorsText.value),
              textColor: AppColor.appBGColorSecondary,
              fontSize: 20,
              fontWeight:  FontWeight.w600,
            ),


            Expanded(
              child: Container(
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: colorsList.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      return _colorCircleWidget(index < colorsList.length ? colorsList[index]: Colors.white , index == colorsList.length);
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _colorCircleWidget(Color color, bool isLast) {
    return Center(
      child: InkWell(
        onTap: isLast ? null : () {
          onColorSelection(color);
        },
        child: Container(
          width: 30,
          height: 30,
          margin: EdgeInsets.only(left: contentPadding, right: contentPadding),
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(15),
              boxShadow: isLast
              ? [
              BoxShadow(color: Colors.black26)
              ]
                  :[]
          ),
          child: isLast
              ? Icon(Icons.add)
              : Container(),
        ),
      ),
    );
  }
}
