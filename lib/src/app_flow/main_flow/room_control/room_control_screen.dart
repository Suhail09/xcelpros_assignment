import 'package:assignment/src/app_flow/main_flow/room_control/room_control_bloc.dart';
import 'package:assignment/src/app_flow/main_flow/room_control/scenes_section_widget.dart';
import 'package:assignment/src/app_flow/main_flow/room_control/intensity_section_widget.dart';
import 'package:assignment/src/constants/app_colors.dart';
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/app_flow/main_flow/room_control/colors_section_widget.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/network/models/room_model.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:assignment/src/ui_elements/container_with_bg_image.dart';
import 'package:flutter/material.dart';
import 'package:assignment/src/helpers/color_lighter_darker.dart';


class RoomControlScreen extends StatefulWidget {
  RoomControlScreen({Key? key, required this.selectedRoom}) : super(key: key);

  @override
  _RoomControlScreenState createState() => _RoomControlScreenState();

  final RoomControlBloc bloc = RoomControlBloc();
  final RoomModel selectedRoom;
}

class _RoomControlScreenState extends State<RoomControlScreen> with SingleTickerProviderStateMixin {

  late AnimationController bulbArmAnimationController;
  late Animation bulbArmHeightAnimation;
  late Animation lightTypeAnimation;
  late Animation colorListAnimation;

  @override
  void initState() {
    bulbArmAnimationController =  AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    bulbArmHeightAnimation = Tween<double>(begin: 80.0, end: 30.0).animate(bulbArmAnimationController);
    lightTypeAnimation = Tween<double>(begin: 400.0, end: 30.0).animate(bulbArmAnimationController);
    colorListAnimation = Tween<double>(begin: 0.0, end: 5.0).animate(bulbArmAnimationController);

    widget.bloc.fetchData().then((value) {
      bulbArmAnimationController.forward();
    });

    bulbArmAnimationController.addListener(() {
      widget.bloc.bulbArmAnimation.sink.add(true);
      widget.bloc.lightTypeUpdatePositionObserver.sink.add(true);
      widget.bloc.colorListAnimatorObserver.sink.add(true);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ContainerWithBackgroundImage(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            headerSection(),
            roomSettingSection(),
          ],
        ),
      ),
    );
  }

  Widget headerSection() {
    return AspectRatio(aspectRatio: 1.25,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(child:
              Container(
                child: Row(
                  children: [
                    roomNameWidget(),
                    bulbJarWidget(),
                  ],
                ),
              )),
              lightTypeList(),
            ],

          ),
        )
    );
  }

  Widget roomNameWidget() {

    List<Widget> childWidgets = [];
    childWidgets.add(InkWell(
        onTap: (){
          bulbArmAnimationController.reverse().then((value) {
            Navigator.pop(context);
          });
        },
        child: Icon(Icons.arrow_back_rounded, color: Colors.white,)),);
    for(String word in widget.selectedRoom.name.split('').toList()) {
      childWidgets.add( _textWidget(title:'$word',
          textColor: Colors.white,
          fontSize: 28,
          fontWeight: FontWeight.bold));
    }

    childWidgets.add(Container(
      margin: EdgeInsets.only(top: 20),
      width: double.maxFinite,
      child: _textWidget(
        fontWeight: FontWeight.w600, fontSize: 15,
        textColor: Colors.orange,
        title: widget.selectedRoom.noOfLights > 1
            ? '${widget.selectedRoom.noOfLights} ' + LanguageHandler().localize(AppStaticTextKeys.lightsText.value)
            : '${widget.selectedRoom.noOfLights} ' + LanguageHandler().localize(AppStaticTextKeys.lightText.value),
      ),
    ));

    return Expanded(
        flex: 4,
        child:   SafeArea(
          child: Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.all(20),
            child: Wrap(
              children: childWidgets,
            ),
          ),
        ));
  }

  Widget bulbJarWidget() {
    return Expanded(
      flex: 5,
      child: StreamBuilder<bool>(
          stream: widget.bloc.bulbArmAnimation.stream,
          builder: (context, snapshot) {
            print(bulbArmHeightAnimation.value);
            return Container(
              padding: EdgeInsets.only(bottom: bulbArmHeightAnimation.value, left: 20, right: 20, ),
              child: Column(
                children: [
                  Expanded(child: Container(
                    width: 5,
                    color: Colors.white,
                  ),),
                  Image.asset(AppImagesKeys.lampJar.value, fit: BoxFit.cover,),
                  bulbWidget(),
                ],
              ),
            );
          }
      ),);
  }

  Widget bulbWidget() {
    return  StreamBuilder<Color>(
      stream: widget.bloc.lightColorSelectionStream,
      initialData: widget.bloc.lightColorSelected,
      builder: (BuildContext context, AsyncSnapshot<Color> snapshot) {
        Color selectedColor = snapshot.data ?? Colors.blueAccent;
        print(widget.bloc.lightIntensity/1000);
        return Container(width: MediaQuery.of(context).size.width*0.07,
          height: (MediaQuery.of(context).size.width*0.07/2),
          decoration:  BoxDecoration(
            color: selectedColor.lighten(amount: widget.bloc.lightIntensity/1000),
            boxShadow: [
              BoxShadow(
                  color:selectedColor.lighten(amount: widget.bloc.lightIntensity/1000),
                  spreadRadius: 5,
                  blurRadius: 20,
                  offset: Offset(0,MediaQuery.of(context).size.width*0.07/5)
              )
            ],
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular((MediaQuery.of(context).size.width*0.07*2).toDouble()),
              bottomRight: Radius.circular((MediaQuery.of(context).size.width*0.07*2).toDouble()),
            ),
          ),
        );
      }
    );
  }

  Widget lightTypeList() {
    return Container(
      height: MediaQuery.of(context).size.width * 0.2,
      width: MediaQuery.of(context).size.width ,
      //color: Colors.green,
      child: StreamBuilder<bool>(
          stream: widget.bloc.lightTypeUpdatePositionObserver.stream,
          builder: (context, snapshot) {
            return Container(
                margin: EdgeInsets.only(left:lightTypeAnimation.value),
                width: double.maxFinite,
                height: double.maxFinite,
                //color: Colors.white,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    lightTypeWidget(title: 'Main Light'),
                    lightTypeWidget(title: 'Desk Light',isSelected: true),
                    lightTypeWidget(title: 'Bed Light'),
                  ],
                )
            );
          }
      ),
    );
  }

  Widget lightTypeWidget({required String title, bool isSelected = false}) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Container(
          width: MediaQuery.of(context).size.width * 0.35,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: isSelected ? AppColor.buttonColor : Colors.white,
            borderRadius: BorderRadius.circular(10)
          ),
          child: _textWidget(title: title,
              textColor: isSelected ? Colors.white : AppColor.buttonColor,
              fontWeight: FontWeight.bold,
              fontSize: 14
          )
      ),

    );
  }

  Widget _textWidget({required String title,
    Color textColor = Colors.black,
    FontWeight fontWeight = FontWeight.w400,
    double fontSize = 14.0
  }) {
    return AppTextWidget(
      title: title,
      textColor: textColor,
      fontSize: fontSize,
      fontWeight: fontWeight,
      maxLines: 2,
    );
  }

  Widget roomSettingSection() {
    return Expanded(
      child: Hero(
        tag: 'test_hero',
        child: Container(
          padding: EdgeInsets.all(20),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40)
              )
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                sliderWidget(),
                colorListWidget(),
                ScenesSectionWidget(),
              ],
            ),
          )
        ),
      ),
    );
  }


  Widget sliderWidget() {
    return StreamBuilder<double>(
        stream: widget.bloc.sliderValueChangeStream,
        initialData: widget.bloc.lightIntensity,
        builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
          return IntensitySectionWidget(onSliderValueChange:widget.bloc.sliderValueDidChange,
            minSliderValue: 100,
            maxSliderValue: 1000,
            totalDivisions: 6,
            sliderValue: snapshot.data ?? 200,
          );
        }
    );
  }

  Widget colorListWidget() {
    return StreamBuilder<bool>(
      stream: widget.bloc.colorListAnimatorObserver.stream,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        return ColorSectionWidget(
            colorsList: widget.bloc.colorsList,
            onColorSelection: widget.bloc.didChangeColor,
          contentPadding: colorListAnimation.value,
        );
      }
    );
  }





  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }
}
