import 'package:assignment/src/app_flow/main_flow/control_panel/control_panel_bloc.dart';
import 'package:assignment/src/app_flow/main_flow/room_control/room_control_screen.dart';
import 'package:assignment/src/constants/app_colors.dart';
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/my_app.dart';
import 'package:assignment/src/network/models/room_model.dart';
import 'package:assignment/src/ui_elements/app_text_widget.dart';
import 'package:assignment/src/ui_elements/container_with_bg_image.dart';
import 'package:flutter/material.dart';

class ControlPanelScreen extends StatefulWidget {
  ControlPanelScreen({Key? key}) : super(key: key);

  @override
  _ControlPanelScreenState createState() => _ControlPanelScreenState();

  final ControlPanelBloc bloc = ControlPanelBloc();
}

class _ControlPanelScreenState extends State<ControlPanelScreen> with RouteAware {

  @override
  void initState() {
    widget.bloc.getRooms();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
  }

  @override
  void didPopNext() {
    widget.bloc.getRooms();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ContainerWithBackgroundImage(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            headerSection(),
            allRoomSection()
          ],
        ),
      ),
    );
  }

  Widget headerSection() {
    return AspectRatio(aspectRatio: 2,
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Expanded(child: Container(
                  child: _textWidget(title: LanguageHandler().localize(AppStaticTextKeys.controlPanelText.value),
                      textColor: Colors.white,
                      fontSize: 26,
                      fontWeight: FontWeight.bold),
                )),
                Image.asset(AppImagesKeys.userImage.value)
              ],
            ),

          ),
        )
    );
  }

  Widget allRoomSection() {
    return Expanded(
      child: Hero(
        tag: 'test_hero',
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.98),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40)
                )
            ),
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(top: 40, left: 20, bottom: 5),
                  alignment: Alignment.centerLeft,
                  child: _textWidget(title: LanguageHandler().localize(AppStaticTextKeys.allRoomsText.value),
                      textColor: AppColor.appBGColorSecondary,
                      fontSize: 18,
                      fontWeight: FontWeight.bold
                  ),
                ),
                Expanded(
                  child: StreamBuilder<List<RoomModel>>(
                      stream: widget.bloc.allRoomsStream,
                      initialData: widget.bloc.allRooms,
                      builder: (BuildContext context, AsyncSnapshot<List<RoomModel>> snapshot) {
                        List<RoomModel> rooms = snapshot.data ?? [];
                        return GridView.builder(
                          padding: EdgeInsets.all(0),
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                          itemBuilder: (_, index) => roomCell(model: rooms[index]),
                          itemCount: rooms.length,
                        );
                      }
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }

  Widget roomCell({required RoomModel model}) {
    return Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(MediaQuery.of(context).size.width *0.1),
        ),
        margin: EdgeInsets.all(15),
        color: Colors.white,
        elevation: 6,
        shadowColor: Colors.black54,
        child: InkWell(
          onTap: (){
            Navigator.push(
              context,
              PageRouteBuilder(
                pageBuilder: (_, __, ___) => RoomControlScreen(selectedRoom: model,),
                transitionDuration: Duration(seconds: 1),
              ),
            );
          },
          child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(child: Container(
                      alignment: Alignment.centerLeft,
                      //color: Colors.blueGrey,
                      child: Image.asset(model.imagePath))),

                  _textWidget(title: model.name, fontWeight: FontWeight.w700, fontSize: 18),

                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: _textWidget(
                      fontWeight: FontWeight.w600, fontSize: 13,
                      textColor: Colors.orange,
                      title: model.noOfLights > 1
                          ? '${model.noOfLights} ' + LanguageHandler().localize(AppStaticTextKeys.lightsText.value)
                          : '${model.noOfLights} ' + LanguageHandler().localize(AppStaticTextKeys.lightText.value),
                    ),
                  )
                ],

              )
          ),
        )
    );
  }

  Widget _textWidget({
    required String title,
    Color textColor = Colors.black,
    FontWeight fontWeight = FontWeight.w400,
    double fontSize = 14.0
  }) {
    return AppTextWidget(
      title: title,
      textColor: textColor,
      fontSize: fontSize,
      fontWeight: fontWeight,
    );
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    routeObserver.unsubscribe(this);
    super.dispose();
  }
}
