
import 'package:assignment/src/network/models/all_rooms_model.dart';
import 'package:assignment/src/network/models/room_model.dart';
import 'package:rxdart/rxdart.dart';
import '../../base_bloc.dart';

class ControlPanelBloc extends BaseBloc{

  final PublishSubject<List<RoomModel>> _allRoomsObserver = PublishSubject<List<RoomModel>>();
  Stream<List<RoomModel>> get allRoomsStream => _allRoomsObserver.stream;
  List<RoomModel> allRooms = <RoomModel>[];

  void getRooms(){
    Future.delayed(Duration(seconds: 1),(){
      AllRoomsModel model = AllRoomsModel();
      allRooms.clear();
      allRooms.addAll(model.roomsList);
      _allRoomsObserver.sink.add(allRooms);
    });
  }
  
  @override
  void dispose() {
    _allRoomsObserver.close();
  }
  
}