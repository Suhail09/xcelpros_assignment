import 'package:assignment/src/constants/app_colors.dart';
import 'package:flutter/material.dart';


ThemeData appDarkTheme() {
  final  ThemeData darkTheme = ThemeData.dark();

  return darkTheme.copyWith(
      primaryColor: Colors.black38,
      accentColor: Color(0xFFFFF8E1),
      buttonTheme: darkTheme.buttonTheme.copyWith(
          buttonColor: AppColor.buttonColor,
          disabledColor: AppColor.disabledButtonColor
      ),
      cursorColor: AppColor.buttonColor
    ///Add value to other properties
  );
}

