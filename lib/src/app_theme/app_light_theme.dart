import 'package:assignment/src/constants/app_colors.dart';
import 'package:flutter/material.dart';

ThemeData appLightTheme() {

  TextTheme textThemeForLightMode(TextTheme base) {
    return base.copyWith(

    );
  }

  ButtonThemeData buttonTextThemeForLightMode(ButtonThemeData base) {
    return base.copyWith(
      buttonColor: AppColor.buttonColor,
      disabledColor: AppColor.disabledButtonColor,
      textTheme: ButtonTextTheme.primary,
    );
  }

  ElevatedButtonThemeData elevatedButtonThemeData() {
    return ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.pressed))
                return AppColor.buttonColor;
              else if (states.contains(MaterialState.disabled))
                return AppColor.disabledButtonColor;
              return  AppColor.buttonColor; // Use the component's default.
              //return Color.fromRGBO(0, 29, 100, 1);
            }),

        textStyle: MaterialStateProperty.resolveWith<TextStyle>(
                (Set<MaterialState> states) {
              if (states.contains(MaterialState.disabled))
                return TextStyle(fontWeight: FontWeight.w700,fontSize: 18, color: Colors.white60);
              else
                return TextStyle(fontWeight: FontWeight.w700,fontSize: 18, color: Colors.white);
            }),
        elevation: MaterialStateProperty.resolveWith<double>(
              (Set<MaterialState> states) {
            return 15.0;
          },),
        shadowColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
              return AppColor.buttonColor.withOpacity(0.6);
            }),
        shape: MaterialStateProperty.resolveWith<RoundedRectangleBorder>(
                (Set<MaterialState> states) {
              return RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10)));
            }),
        minimumSize: MaterialStateProperty.resolveWith<Size>(
                (Set<MaterialState> states) {
              return  Size(200, 50);
            }),
      ),
    );
  }


  final  ThemeData lightTheme = ThemeData.light();
  return lightTheme.copyWith(
    accentColor: Color(0xFFFFF8E1),
    primaryColor: AppColor.buttonColor,
    buttonTheme: buttonTextThemeForLightMode(lightTheme.buttonTheme),
    cursorColor: AppColor.buttonColor,
    elevatedButtonTheme: elevatedButtonThemeData(),
    ///Provide value to other properties if needed
  );
}