
import 'package:flutter/cupertino.dart';

extension ColorLightening on Color {
  Color lighten({double amount = .1}) {
    assert(amount >= 0 && amount <= 1); //// Should ranges between 0.0 to 1.0
    final hsl = HSLColor.fromColor(this);
    return hsl.withAlpha(amount).toColor();
  }
}
