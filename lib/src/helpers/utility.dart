
import 'package:flutter/cupertino.dart';

class Utility {
  static const double _maxFontScaleFactor = 1.0;
  static const double _minFontScaleFactor = 0.3;
  static double _textScaleFactor = 0.4;

  static double get textScaleFactor => _textScaleFactor;


  static double fontScaleFactor({required BuildContext context}) {
    final double textScaleFactor = (MediaQuery.of(context).size.width / MediaQuery.of(context).size.height) * Utility.devicePixelRation(context);
    Utility._textScaleFactor = textScaleFactor;
    print('Scale factor for device:-  $textScaleFactor');

    return textScaleFactor >= _maxFontScaleFactor
        ? _maxFontScaleFactor
        : textScaleFactor < _minFontScaleFactor
        ? _minFontScaleFactor
        : textScaleFactor;
  }

  static double devicePixelRation(BuildContext context) {
    return MediaQuery.of(context).devicePixelRatio;
  }
}