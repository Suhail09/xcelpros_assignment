
class RoomModel {

  RoomModel({
    required this.name,
    required this.imagePath,
    this.noOfLights = 1
  });

  late String name;
  late int noOfLights;
  late String imagePath;
}