
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:flutter/material.dart';

class ScenesModel {

  ScenesModel() {

    scenesList = <AScene>[];
    scenesList.add(AScene(
        title: LanguageHandler().localize(AppStaticTextKeys.lightsText.value),
      sceneGradientColors: [
        Colors.orangeAccent.shade400,
        Colors.orange.shade300,
        Colors.orange.shade200,
      ]
    ));

    scenesList.add(AScene(
        title: LanguageHandler().localize(AppStaticTextKeys.partyText.value),
        sceneGradientColors: [
          Colors.purple.shade400,
          Colors.purple.shade300,
          Colors.purple.shade200,
        ]
    ));

    scenesList.add(AScene(
        title: LanguageHandler().localize(AppStaticTextKeys.relaxText.value),
        sceneGradientColors: [
          Colors.blue.shade400,
          Colors.blue.shade300,
          Colors.blue.shade200,
        ]
    ));

    scenesList.add(AScene(
        title: LanguageHandler().localize(AppStaticTextKeys.funText.value),
        sceneGradientColors: [
          Colors.green.shade400,
          Colors.green.shade300,
          Colors.green.shade200,
        ]
    ));

  }

  late List<AScene> scenesList;
}


class AScene {
  AScene({required this.title, required this.sceneGradientColors});

  late String title;
  late List<Color> sceneGradientColors;
}