
import 'package:assignment/src/constants/app_images_keys.dart';
import 'package:assignment/src/constants/app_static_text_keys.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/network/models/room_model.dart';

class AllRoomsModel {

  late List<RoomModel> roomsList;

  AllRoomsModel() {
    roomsList = <RoomModel>[];
    roomsList.add(RoomModel(
        name: LanguageHandler().localize(AppStaticTextKeys.bedRoomText.value,),
        imagePath: AppImagesKeys.bedroomImage.value, noOfLights: 4));
    roomsList.add(RoomModel(
        name: LanguageHandler().localize(AppStaticTextKeys.kitchenText.value,),
        imagePath: AppImagesKeys.kitchenImage.value,noOfLights: 5));
    roomsList.add(RoomModel(
        name: LanguageHandler().localize(AppStaticTextKeys.bathroomText.value,),
        imagePath: AppImagesKeys.bathroomImage.value, noOfLights: 1));
    roomsList.add(RoomModel(
        name: LanguageHandler().localize(AppStaticTextKeys.outdoorText.value,),
        imagePath: AppImagesKeys.outdoorImage.value,noOfLights: 5));
    roomsList.add(RoomModel(
        name: LanguageHandler().localize(AppStaticTextKeys.balconyText.value,),
        imagePath: AppImagesKeys.balconyImage.value,noOfLights: 2));
  }
}