

import 'package:assignment/src/helpers/enumeration.dart';

class AppImagesKeys extends Enum<String> {
  AppImagesKeys(String value) : super(value);

  static AppImagesKeys backgroundImage = AppImagesKeys('assets/images/background_image.png');
  static AppImagesKeys backgroundImage2 = AppImagesKeys('assets/images/background_image_2.png');

  static AppImagesKeys balconyImage = AppImagesKeys('assets/images/balcony.png');
  static AppImagesKeys bathroomImage = AppImagesKeys('assets/images/bathroom.png');
  static AppImagesKeys bedroomImage = AppImagesKeys('assets/images/bedroom.png');
  static AppImagesKeys kitchenImage = AppImagesKeys('assets/images/kitchen.png');
  static AppImagesKeys outdoorImage = AppImagesKeys('assets/images/outdoor.png');
  static AppImagesKeys userImage = AppImagesKeys('assets/images/user_image.png');
  static AppImagesKeys lampJar = AppImagesKeys('assets/images/lamp_jar.png');
  static AppImagesKeys bulbWhite = AppImagesKeys('assets/images/bulb_white.png');
  static AppImagesKeys bulbGlowed = AppImagesKeys('assets/images/bulb_glowed.png');
//
}