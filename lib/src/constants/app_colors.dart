import 'package:flutter/material.dart';

mixin AppColor {
  static Color appBGColorPrimary = const Color.fromRGBO(3, 31, 39, 1);
  static Color appBGColorSecondary = const Color.fromRGBO(5, 67, 84, 1);
//  static Color dodgerBlueColor = const Color.fromRGBO(30, 49, 157, 1);
  static Color disabledButtonColor = const Color.fromRGBO(153, 203, 255, 1);
  static Color buttonColor = const Color.fromRGBO(0, 167, 155, 1);
}