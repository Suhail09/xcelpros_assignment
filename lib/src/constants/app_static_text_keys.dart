
import 'package:assignment/src/helpers/enumeration.dart';

class AppStaticTextKeys extends Enum<String> {
  AppStaticTextKeys(String value) : super(value);

  static AppStaticTextKeys genericErrorMessage = AppStaticTextKeys('generic_error_message');
  static  AppStaticTextKeys passwordText = AppStaticTextKeys('password_text');
  static  AppStaticTextKeys userNameText = AppStaticTextKeys('user_name_text');
  static  AppStaticTextKeys signUpText = AppStaticTextKeys('sign_up_text');
  static  AppStaticTextKeys signInText = AppStaticTextKeys('sign_in_text');
  static  AppStaticTextKeys dontHaveAccountMessage = AppStaticTextKeys('dont_have_account_message');
  static  AppStaticTextKeys userNameRequiredMessage = AppStaticTextKeys('user_name_required');
  static  AppStaticTextKeys userNameInvalidMessage = AppStaticTextKeys('user_name_invalid');
  static  AppStaticTextKeys passwordRequiredMessage = AppStaticTextKeys('password_required_message');
  static  AppStaticTextKeys passwordStrengthErrorMessage = AppStaticTextKeys('password_strength_error_message');

  static  AppStaticTextKeys backText = AppStaticTextKeys('back_text');
  static  AppStaticTextKeys iHaveAcceptedTheText = AppStaticTextKeys('i_have_accepted_the_text');
  static  AppStaticTextKeys termsAndConditionText = AppStaticTextKeys('terms_&_condition_text');

  static  AppStaticTextKeys welcomeText = AppStaticTextKeys('welcome_text');
  static  AppStaticTextKeys toRoomControlText = AppStaticTextKeys('to_room_control_text');
  static  AppStaticTextKeys createNewAccountText = AppStaticTextKeys('create_new_account_text');
  static  AppStaticTextKeys emailText = AppStaticTextKeys('email_text');
  static  AppStaticTextKeys emailRequired = AppStaticTextKeys('email_required');
  static  AppStaticTextKeys emailInvalid = AppStaticTextKeys('email_invalid');

  static  AppStaticTextKeys controlPanelText = AppStaticTextKeys('control_panel_text');
  static  AppStaticTextKeys allRoomsText = AppStaticTextKeys('all_rooms_text');
  static  AppStaticTextKeys bedRoomText = AppStaticTextKeys('bed_room_text');
  static  AppStaticTextKeys kitchenText = AppStaticTextKeys('kitchen_text');
  static  AppStaticTextKeys livingRoomText = AppStaticTextKeys('living_room_text');
  static  AppStaticTextKeys bathroomText = AppStaticTextKeys('bathroom_text');
  static  AppStaticTextKeys outdoorText = AppStaticTextKeys('outdoor_text');
  static  AppStaticTextKeys balconyText = AppStaticTextKeys('balcony_text');
  static  AppStaticTextKeys lightText = AppStaticTextKeys('light_text');
  static  AppStaticTextKeys lightsText = AppStaticTextKeys('lights_text');
  static  AppStaticTextKeys intensityText = AppStaticTextKeys('intensity_text');
  static  AppStaticTextKeys colorsText = AppStaticTextKeys('colors_text');
  static  AppStaticTextKeys scenesText = AppStaticTextKeys('scenes_text');
  static  AppStaticTextKeys birthdayText = AppStaticTextKeys('birthday_text');
  static  AppStaticTextKeys partyText = AppStaticTextKeys('party_text');
  static  AppStaticTextKeys relaxText = AppStaticTextKeys('relax_text');
  static  AppStaticTextKeys funText = AppStaticTextKeys('fun_text');
  static  AppStaticTextKeys letsGetYouStarted = AppStaticTextKeys('lets_get_you_started');


}