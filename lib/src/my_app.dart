
import 'package:assignment/src/app_flow/auth_flow/login/login_screen.dart';
import 'package:assignment/src/app_flow/main_flow/control_panel/control_panel_screen.dart';
import 'package:assignment/src/helpers/language_loader.dart';
import 'package:assignment/src/helpers/utility.dart';
import 'package:flutter/material.dart';
import 'app_flow/loader_widget.dart';
import 'app_theme/app_dark_theme.dart';
import 'app_theme/app_light_theme.dart';

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Assignment',
      debugShowCheckedModeBanner: false,
      darkTheme: appDarkTheme(),
      theme: appLightTheme(),
      //themeMode: ThemeMode.system,
      themeMode: ThemeMode.light,
      navigatorObservers: [routeObserver],
      home: FutureBuilder<Map<String, dynamic>>(
        future: LanguageHandler().loadLanguage(languageCode: 'en'),
        builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> futureData) {
          if(futureData.hasData) {
            return LoginScreen();
          }
          else
            return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),),
            );
        },
      ),
    );
  }



}